from django.contrib import admin
from django.urls import path, include
from mainapp import views
from mainapp.views import About, Explore_View,Faqs,Explore,Index,ProductDetail
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import handler404
from django.views.static import serve
from django.conf.urls import url
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', Index.as_view()),
    path('about/', About.as_view()),
    path('contact/', views.contact, name='contact'),
    path('faqs/', Faqs.as_view()),
    path('explore/', Explore.as_view()),
    # path('cool/', views.Error_404),  # For Debugging 404 page
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
    path('<slug:slug>/', ProductDetail.as_view(), name='product_detail1'),
    path('explore/<slug:slug>/', Explore_View.as_view()),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = 'mainapp.views.Error_404'