from django.db import models
from django.shortcuts import render
from django.urls import reverse
from django.template.defaultfilters import slugify
from datetime import datetime
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver

class Category_Section(models.Model):
    category_name = models.CharField(max_length=50, default="")
    starting_price = models.CharField(max_length=50, default="")
    category_description = models.CharField(max_length=300,default="")
    image = models.ImageField(upload_to="img/category")
    slug = models.SlugField(max_length=100)

    def __str__(self):
        return self.category_name
    def get_absolute_url(self):
        return reverse("category_detail", kwargs={"slug": self.slug})
    
class SubCategory(models.Model):
    category = models.ForeignKey(Category_Section,default=None,on_delete=models.CASCADE)
    sub_category = models.CharField(max_length=50, default="")
    def __str__(self):
        return self.sub_category

class Product(models.Model):
    product_name = models.CharField(max_length=50)
    brand = models.CharField(max_length=50)
    product_category = models.ForeignKey(Category_Section, default=None, on_delete=models.CASCADE)
    sub_category = models.ForeignKey(SubCategory,default=None,on_delete=models.CASCADE)
    description = models.CharField(max_length=300)
    published_date = models.DateField(default=datetime.today)
    image = models.ImageField(upload_to="img", default="",blank=True)
    slug = models.SlugField(max_length=200,unique=True)

    def __str__(self):
        return self.product_name
    
    def get_absolute_url(self):
        return reverse('product_detail1', kwargs={'slug': self.slug})

class KeyPoints(models.Model):
    post = models.ForeignKey(Product, default=None, on_delete=models.CASCADE)
    key_point = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.post.product_name
class Multi_img(models.Model):
    post = models.ForeignKey(Product, default=None, on_delete=models.CASCADE)
    supporting_image = models.ImageField(upload_to="img/supporting")

    def __str__(self):
        return self.post.product_name

class Explore_img(models.Model):
    post = models.ForeignKey(Product, default=None, on_delete=models.CASCADE)
    supporting_image = models.ImageField(upload_to="img/explore")

    def __str__(self):
        return self.post.product_name

class FAQ(models.Model):
    title = models.CharField(max_length=150, default="")

    def __str__(self):
        return self.title
  
class QNA(models.Model):
    post = models.ForeignKey(FAQ, default=None, on_delete=models.CASCADE)
    Question = models.CharField(max_length=500, default="")
    Answer = models.TextField(default="")

    def __str__(self):
        return self.Question

@receiver(post_delete)
def delete_files_when_row_deleted_from_db(sender, instance, **kwargs):
    for field in sender._meta.concrete_fields:
        if isinstance(field,models.ImageField):
            instance_file_field = getattr(instance,field.name)
            delete_file_if_unused(sender,instance,field,instance_file_field)

@receiver(pre_save)
def delete_files_when_file_changed(sender,instance, **kwargs):
    if not instance.pk:
        return
    for field in sender._meta.concrete_fields:
        if isinstance(field,models.ImageField):
            try:
                instance_in_db = sender.objects.get(pk=instance.pk)
            except sender.DoesNotExist:
                return
            instance_in_db_file_field = getattr(instance_in_db,field.name)
            instance_file_field = getattr(instance,field.name)
            if instance_in_db_file_field.name != instance_file_field.name:
                delete_file_if_unused(sender,instance,field,instance_in_db_file_field)
  
def delete_file_if_unused(model,instance,field,instance_file_field):
    dynamic_field = {}
    dynamic_field[field.name] = instance_file_field.name
    other_refs_exist = model.objects.filter(**dynamic_field).exclude(pk=instance.pk).exists()
    if not other_refs_exist:
        instance_file_field.delete(False)