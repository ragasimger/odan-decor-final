from django.contrib import admin

from .models import FAQ, QNA, Category_Section, Product, Multi_img, SubCategory, Explore_img,KeyPoints

admin.site.register(Category_Section)
admin.site.register(SubCategory)

class Key_Points(admin.StackedInline):
    model = KeyPoints
class PostImageAdmin(admin.StackedInline):
    model = Multi_img
class PostExploreAdmin(admin.StackedInline):
    model = Explore_img

 
@admin.register(Product)
class PostAdmin(admin.ModelAdmin):
    inlines = [Key_Points,PostImageAdmin,PostExploreAdmin]
 
    class Meta:
       model = Product
 
@admin.register(KeyPoints)
class Key_Points(admin.ModelAdmin):
    pass
@admin.register(Multi_img)
class PostImageAdmin(admin.ModelAdmin):
    pass
@admin.register(Explore_img)
class PostExploreAdmin(admin.ModelAdmin):
    pass

class PostQuestionAdmin(admin.StackedInline):
    model = QNA
@admin.register(FAQ)
class PostAdmin(admin.ModelAdmin):
    inlines = [PostQuestionAdmin]
    class Meta:
       model = FAQ