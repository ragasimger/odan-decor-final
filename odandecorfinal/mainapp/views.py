from django.shortcuts import render
from datetime import date
from django import template
from django.core.mail.message import EmailMultiAlternatives
from .models import FAQ, QNA, Product, Multi_img, Category_Section
from django.conf import settings
from django.views.generic import TemplateView,DetailView,ListView


# Email sending library starts
from django.core.mail import send_mail, EmailMessage
from django.template.loader import render_to_string
from django.utils.html import strip_tags
# Email sending library ends

class Index(TemplateView):
    model = Product
    catmodel = Category_Section
    template_name = "index.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args,**kwargs)
        context["product"] = self.model.objects.all()
        context["cat"] = self.catmodel.objects.all()
        return context
     
def contact(request):
    if request.method == "POST":
        today = date.today()
        name = request.POST['name']
        emailname = request.POST['email']
        subject = request.POST['subject']
        message = request.POST['message']
        # template = render_to_string('thankyou.html', {'name':name})
        context = {'name':name, 'today':today}
        html_content = render_to_string('emailthanks.html', context)
        template = strip_tags(html_content)
        send_mail(
            'Message from' + '  ' + name + '  ' + 'on subject:  ' + subject,
            "His/her email: " + emailname + "\n\n Message starts from here:\n\n" + message,
            emailname,
            ['odandecor@gmail.com'],
            fail_silently = False,
        )
        email = EmailMultiAlternatives(
            'Message from Odan Decor',
            template,
            settings.EMAIL_HOST_USER,
            [emailname],
        )
        email.attach_alternative(html_content,"text/html")
        email.fail_silently = False
        email.send()

        params = {'name':name}
        return render(request,'contact.html',params)
    else:
        return render(request,"contact.html")

class About(TemplateView):
    template_name = "about.html"

class Faqs(TemplateView):
    model = QNA
    template_name = "faqs.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args,**kwargs)
        context["faq"] = self.model.objects.all()
        return context

class Explore(TemplateView):
    template_name = "explore.html"
    model = Product

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args,**kwargs)
        context["product"] = self.model.objects.all()
        return context

class ProductDetail(DetailView):
    context_object_name = 'products'
    model = Product
    template_name = "productdetail.html"
    
class Explore_View(DetailView):
    context_object_name = 'explore'
    model = Product
    template_name = "shop.html"

# ### For debugging 404 page
# def Error_404(request):
#     return render(request,"404.html", status=404)
# ###Debugging 404 ends

def Error_404(request,exception):
    return render(request,"404.html", status=404)

