# Generated by Django 3.2.3 on 2021-07-17 16:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0012_remove_productimage_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='productimage',
            name='slug',
            field=models.SlugField(default=2, max_length=200, unique=True),
            preserve_default=False,
        ),
    ]
